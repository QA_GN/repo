# README #

### What is this repository for? ###

* This repository is created for storing version of automated Postman smoke tests on Gelato Network project and created by QA.
* ReadMe doc ver 1.2.1

### How do I get set up? ###

1) Install Git Bash https://git-scm.com/downloads

2) Create a folder in your local PC where you will store your repository

3) Run Git Bash

4) Change work directory in Git bash to the folder from step 2.
> cd <path>

e.g.:

>cd C:/users/admin/Deocuments/GIT/repo

5) Run next command:
> git init

6) Get this repo from bitbucket with next command. Link you can find in the top of this page.
> git clone <link>

7) Upload the new files to cloned repo in your local PC.
Please, use the folders structure and store the specific collections to appropriate folder
> If there are no folder just create new folder on your local PC and save files in the created folder

8) The next command will highlight with red the modified or new file 
> git status

9) When you updated the file locally, add files to the local repo with command 
> git add -A

10) After file are added the next comand will highlight with green the added files
> git status

11) After changing of the files in the repo make a snapshot of the files. Use next command
> git commit -m "comment"

Don't forget to leave a comment which describe changes.

12) To update online repository you need to run next command:
> git push -u origin master

### Who do I talk to? ###

* Contact with QA team
* Other community or team contact